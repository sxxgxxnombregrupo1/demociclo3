/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.dao;

import com.misiontic.DemoCiclo3.model.Producto;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MisionTIC
 */
public interface ProductoDao extends CrudRepository<Producto,Integer> {
    
@Transactional(readOnly=true)
@Query(value="SELECT * FROM producto WHERE nombreProducto like %:name%", nativeQuery = true)
public List<Producto> findByName(@Param("name") String name);
}
  