/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.dao;

import com.misiontic.DemoCiclo3.model.Producto;
import com.misiontic.DemoCiclo3.model.Transaccion;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MisionTIC
 */
public interface TransaccionDao extends CrudRepository<Transaccion,Integer> {
    @Transactional(readOnly=true)

//Nota: El formato de fecha en mysql es año,mes,dia
//Se agrega la funcion str_to_date debido a que el tipo de la columna fechaTransaccion 
//es varchar, es necesario castear al tipo fecha para lograr hacer el between 
@Query(value="SELECT * FROM transaccion WHERE str_to_date(fechaTransaccion, '%d-%m-%Y') between :fechaIni and :fechaFin", nativeQuery = true)
public List<Transaccion> findByDate(@Param("fechaIni") String fechaIni, @Param("fechaFin") String fechaFin);

}
