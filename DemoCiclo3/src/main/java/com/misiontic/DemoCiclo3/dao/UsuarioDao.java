/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.dao;

import com.misiontic.DemoCiclo3.model.Producto;
import com.misiontic.DemoCiclo3.model.Usuario;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MisionTIC
 */
public interface UsuarioDao extends CrudRepository<Usuario,Integer> {
    
    @Transactional(readOnly=true)
    @Query(value="SELECT * FROM usuario WHERE activo = 1 and usuario = :user and clave = :password", nativeQuery = true)
    public Usuario login(@Param("user") String usuario, @Param("password") String clave);

}
