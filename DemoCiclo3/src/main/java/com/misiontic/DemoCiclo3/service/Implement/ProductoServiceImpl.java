/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.service.Implement;

import com.misiontic.DemoCiclo3.dao.ProductoDao;
import com.misiontic.DemoCiclo3.model.Producto;
import com.misiontic.DemoCiclo3.service.ProductoService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MisionTIC
 */
@Service
public class ProductoServiceImpl implements ProductoService {
    @Autowired
private ProductoDao productoDao;

@Override
@Transactional(readOnly=false)
public Producto save(Producto producto){
return productoDao.save(producto);
}

@Override
@Transactional(readOnly=false)
public void delete(Integer id){
productoDao.deleteById(id);
}

@Override
@Transactional(readOnly=true)
public Producto findById(Integer id){    
return productoDao.findById(id).orElse(null);
}

@Override
@Transactional(readOnly=true)
public List<Producto> findAll(){
return (List<Producto>) productoDao.findAll();
}

@Override
@Transactional(readOnly=true)
public List<Producto> findByName(String nameProduct){
return productoDao.
        findByName(nameProduct);
}


}
