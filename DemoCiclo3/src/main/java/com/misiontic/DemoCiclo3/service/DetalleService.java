/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.service;

import com.misiontic.DemoCiclo3.model.Detalle;
import java.util.List;

/**
 *
 * @author MisionTIC
 */
public interface DetalleService {
    public Detalle save(Detalle detalle);
    public void delete(Integer id);
    public Detalle findById(Integer id);
    public List<Detalle> findAll();
}
