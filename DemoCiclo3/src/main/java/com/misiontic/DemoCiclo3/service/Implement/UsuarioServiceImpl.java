/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.service.Implement;

import Utilities.Cypher;
import com.misiontic.DemoCiclo3.dao.ProductoDao;
import com.misiontic.DemoCiclo3.dao.UsuarioDao;
import com.misiontic.DemoCiclo3.model.Producto;
import com.misiontic.DemoCiclo3.model.Usuario;
import com.misiontic.DemoCiclo3.service.ProductoService;
import com.misiontic.DemoCiclo3.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MisionTIC
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {
    @Autowired
private UsuarioDao usuarioDao;

@Override
@Transactional(readOnly=false)
public Usuario save(Usuario usuario){
usuario.setClave(new Cypher().strToMD5(usuario.getClave()));
return usuarioDao.save(usuario);
}

@Override
@Transactional(readOnly=false)
public void delete(Integer id){
usuarioDao.deleteById(id);
}

@Override
@Transactional(readOnly=true)
public Usuario findById(Integer id){    
return usuarioDao.findById(id).orElse(null);
}

@Override
@Transactional(readOnly=true)
public List<Usuario> findAll(){
return (List<Usuario>) usuarioDao.findAll();
}

@Override
@Transactional(readOnly=true)
public Usuario login(String userName, String password){
return usuarioDao.
        login(userName, new Cypher().strToMD5(password));
}


}
