/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.service;

import com.misiontic.DemoCiclo3.model.Producto;
import java.util.List;

/**
 *
 * @author MisionTIC
 */
public interface ProductoService {
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
    public List<Producto> findByName(String nameProduct);
}
