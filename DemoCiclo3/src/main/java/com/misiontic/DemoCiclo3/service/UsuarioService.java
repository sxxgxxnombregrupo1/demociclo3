/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.misiontic.DemoCiclo3.service;

import com.misiontic.DemoCiclo3.model.Producto;
import com.misiontic.DemoCiclo3.model.Usuario;
import java.util.List;

/**
 *
 * @author MisionTIC
 */
public interface UsuarioService {
    public Usuario save(Usuario usuario);
    public void delete(Integer id);
    public Usuario findById(Integer id);
    public List<Usuario> findAll();
    public Usuario login(String userName, String password);
}
