/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilities;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author MisionTIC
 */
public class Cypher {
    public String strToMD5(String text){
        MessageDigest md5 =null;
        try
        {
            md5 = MessageDigest.getInstance("MD5");
            md5.update(StandardCharsets.UTF_8.encode(text));
        }
        catch(Exception ex)
        {
        }
        return String.format("%032x", new BigInteger(1, md5.digest()));
    }
}
